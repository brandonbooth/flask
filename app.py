from flask import Flask, render_template, request, redirect, url_for
import pandas as pd 
import json, os


def filter_dict(df, d):
    q = " and ".join([f"{k}=='{v}'" for k, v in d.items()])
    return df.query(q)

#test2 = filter_dict(df1, testd)

def comp_df(df):
    return df[['competed','fy','amt']].groupby(['competed','fy']).sum().reset_index()


def to_j(df, ns = ['key', 'value']):
    df_d = df 
    df_d.columns = ns 
    return df_d.to_dict(orient='records')


df1 = pd.read_csv('BIURL.csv')
df1 = df1.drop(columns=['agency_url','department_url','reasonNotCompeted','AGENCY_NAME','typeOfContractPricing'])



df1 = df1.rename(columns={
    'psc2012_name':'psc',
    'department_name':'dept',
    'obligations':'amt',
    'dod_civilian':'dod_civ',
    'co_size': 'sb'
})

df = df1.copy()





comp = to_j(comp_df(df1), ['competed','fy','amt'])


depts = list(df['dept'].unique())

app = Flask(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = True

@app.route('/dept', methods=['POST'])
def dpt():
    if request.method == 'POST':

        dept = request.get_json().get('dept')
        dept = dept.replace("_"," ").replace("-",",")        
        
        if dept == "All Departments":
            df = df1
        else:
            df = df1.loc[df1['dept']==dept]

        dept_data = to_j(df[['fy','amt']].groupby('fy').sum().reset_index())
        return json.dumps({'dept': dept_data})
      
          

@app.route('/')
def home():
    dept_data = to_j(df[['fy','amt']].groupby('fy').sum().reset_index())
    return render_template('index.html', 
        depts=depts,
        dept_data=dept_data,
        comp=comp)


if __name__ == '__main__':
    app.run(debug=True)

