
function runPlots() {
    barPlot({selector:'#cell1', data: dept_data, title: "Obligations by Fiscal Year"});

    linePlot({selector: '#cell2', data: comp, ks: ['competed','fy'], vs:'amt' });
};

function safeIt(val) {
    return val.replaceAll(" ","_").replaceAll(",","-")
}

function refresh() {
    var h = new Headers({"Content-Type": "application/json"});

    var dpt = safeIt(d3.select("#dept-select").property("value"));
    console.log(dpt);
  
    fetch("http://localhost:5000/dept", {
            method: 'POST',
            body: JSON.stringify({"dept": dpt}),
            headers: h
        })
        .then(response=>response.json())
        .then(d => {
            dept_data = d.dept;
            runPlots();
        });

};


function deptDrop(){
    var dd = d3.select("#dept-select");

    var ds = ['All Departments'].concat(depts);
    

    dd.selectAll("option")
        .data(ds)
        .enter()
        .append("option")
        .attr("value", d=>d)
        .text(d=>d);

    dd.on("change", function(d) {
        refresh();                
	});
    
};


function mouseout() {
    d3.select("#tooltip")
      .attr("class", "hidden")
      .html("");
};

function mousemove() {
    d3.select("#tooltip")
    .style('left', d3.event.pageX + 10 + 'px')
    .style('top', d3.event.pageY - 10 + 'px')
}
    
function mouseover(d) {
    d3.select("#tooltip")
        .html(formatDollar(d.value))
        .attr("class","tooltip")
        ;
};

function formatDollar(val, digits=0) {
    if (typeof val == 'number') {
      return '$' + val.toFixed(digits).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    } else {
      return val;
    }
};



function unroll(rollup, keys, label = "value", p = {}) {
    return Array.from(rollup, ([key, value]) => 
      value instanceof Map 
        ? unroll(value, keys.slice(1), label, Object.assign({}, { ...p, [keys[0]]: key } ))
        : Object.assign({}, { ...p, [keys[0]]: key, [label] : value })
    ).flat();
  };

function groupSum(data, keys, value) {
    const rolled = d3.rollup(data, v => d3.sum(v, d => d[value]), ...keys.map(k => d => d[k]));
    return unroll(rolled, keys, value);
  };



function barPlot({...vals}={}) {

    var {selector,
        data,
        xspace = svg_size.xspace,
        w=svg_size.width,
        h=svg_size.height,
        titlespace= svg_size.titlespace,
        title = ''} = vals;

 
    var width = w - margin.right - margin.left - xspace;
    var height = h - margin.top - margin.bottom - titlespace;

    d3.select(selector).selectAll("svg").remove();
    d3.select(selector).selectAll("table").remove();


    // set x scale - this is the category
    var xscale = d3.scaleBand()
        .domain(data.map(d => d.key))
        .range([0,width])
        .padding(0.1);
  
    // set y scale - equal to the highest value
    var yscale = d3.scaleLinear()
        .domain([0, d3.max(data, d=>d.value)])
        .range([height, 0]);
  
    // append svg using height and width
    var chart = d3.select(selector)
        .append("svg")
            .attr("width", w)
            .attr("height", h)
            .style("background-color","white")
            .style("border","1px solid black")


        .append("g")
            .attr("transform", "translate(" + xspace + ", " + (margin.top+titlespace) + ")")
            ;

    var div = d3.select("#tooltip");
  
    // add rectangles for each category
    var bars = chart.selectAll("rect")
        .data(data)
        .enter()
        .append("rect")
        .attr("height", d=> height - yscale(d.value))
        .attr("width",xscale.bandwidth())
        .attr("x", d=>xscale(d.key))
        .attr("y", d=>yscale(d.value))
        .attr("fill", "steelblue")
        .on("mouseover", d =>  mouseover(d))
        .on("mousemove", d => mousemove())
        .on("mouseout", d => mouseout())
        ;
  
    chart.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(xscale))
        ;
  
    // create y axis
    var y_axis = d3.axisLeft()
        .scale(yscale)
        .tickFormat(d => d/1000000);
  
    // add y axis to figure.
    chart.append("g")
        .call(y_axis);

    // add title
    chart.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0 - (titlespace/2))
        .attr("text-anchor", "middle")  
        .style("font-size", "16px") 
        .style("text-decoration", "underline")  
        .text(title);

  
  };


// input passed from groupSum function and then nested by key.
function linePlot({...vals}={}) {

    var {selector,
        data,
        ks,
        vs,
        xspace = svg_size.xspace,
        w=svg_size.width,
        h=svg_size.height,
        titlespace=svg_size.titlespace,
        title = ''} = vals;

    d3.select(selector).selectAll("svg").remove();

    var k1 = ks[0];
    var k1list = _.uniq(_.map(data, k1)).sort();
    var k2 = ks[1];
    var k2list = _.uniq(_.map(data, k2)).sort();

    var width = w - margin.right - margin.left - xspace;
    var height = h - margin.top - margin.bottom - titlespace;

    var data1 = groupSum(data, ks, vs);
    

    var data2 = d3.nest().key(d=>d[k1]).entries(data1);
    
    vmax = d3.max(data1, d => d[vs]);

    // data1 has list of values so we use that to determine scale.
 
    var xScale = d3.scaleBand()
        .domain(k2list)
        .range([0, width]);

    var yScale = d3.scaleLinear()
        .domain([0, vmax])
        .range([height, 0]);

    var yScale1 = d3.scaleLinear()
        .domain([0, (vmax/1000000000)])
        .range([height, 0]);

    var color = d3.scaleOrdinal()
        .domain(k1list)
        .range(d3.schemeCategory10)
        ;


    var svg = d3.select(selector)
        .append("svg")
            .attr("width", w)
            .attr("height", h)
            .style("background-color","white")
            .style("border","1px solid black")
        .append("g")
            .attr("transform", "translate(" + xspace + ", " + (margin.top+titlespace) + ")")
        ;

    svg.selectAll(".line")
        .append("g")
        .attr("class", "line")
        .data(data2)
        .enter()
        .append("path")
        .attr("fill", "none")
        .attr("stroke", d => color(d.key))
        .attr("stroke-width", 2)
        .attr("d", function (d) {
            return d3.line()
                .x(d => xScale(d[k2]) + margin.left)
                .y(d => yScale(d[vs]))
                (_.sortBy(d.values, e=>e[k2]));
                
        })
        ;

    // add axes
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(xScale));

    svg.append("g")
        .call(d3.axisLeft(yScale1));

    // add legend
    var legend = svg.selectAll('.legend')
        .data(k1list)
        .enter()
        .append(".legend");

    legend.append('circle')
        .attr('cx', width + 20)
        .attr('cy', 20)
        .attr('r', 7)
        .style('fill', d => color(d)
        );

};



  